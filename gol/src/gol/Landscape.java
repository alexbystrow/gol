package gol;

import gol.Mode.Mode;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPanel;

/**
 *
 * @author bystrow
 */
public class Landscape extends JPanel implements Runnable {

    // field params
    public int width = 0;
    public int height = 0;
    public final int gridSizeX = 400;
    public final int gridSizeY = 200;

    // initial created live cells
    public int liveCells = 8000;

    // population params
    public int overpopulation = 3;
    public int underpopulation = 2;
    public int birthcontrol = 3;
    public int cellIsOld = 10;

    // runtime params
    public int generation = 0;
    public final int sleep = 33;

    // cell array
    public Cell[][] cells = new Cell[gridSizeX + 1][gridSizeY + 1];

    // game mode
    private Mode mode;
    
    Landscape(Mode mode, int width, int height) {
        this.width = width;
        this.height = height;
        this.mode = mode;
        
        mode.setup(this);

        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                int x = e.getLocationOnScreen().x;
                int y = e.getLocationOnScreen().y;

                bringToLive(x, y);
            }

        });
    }

    /**
     * @param x
     * @param y
     */
    private void bringToLive(int x, int y) {

        int cellWidth = (width / gridSizeX);
        int cellHeight = (height / gridSizeY);
        
        x = x / cellWidth;
        y = y / cellHeight;

        x -= 1 * cellWidth;
        y -= 4 * cellHeight;

        for (int i = 0; i <= 10; i++) {
            for (int j = 0; j <= 10; j++) {
                if ((int)(Math.random() * 100) % 2 == 0 ) {
                    cells[x + i][y + j].dead = false;
                    cells[x + i][y + j].lifetime = 0;
                }
            }
        }

    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawCreatures(g);
        drawGrid(g);
    }

    /**
     * paint cells according to its life status
     *
     * @param g
     */
    private void drawCreatures(Graphics g) {

        int cellWidthX = width / gridSizeX;
        int cellWidthY = height / gridSizeY;

        for (int x = 0; x < gridSizeX; x++) {
            for (int y = 0; y < gridSizeY; y++) {

                int color = 255;

                // dead / not dead
                if (cells[x][y].dead == true) {
                    color = 40;
                }

                color -= (cells[x][y].lifetime * 10);
                if (color <= 0) {
                    color = 1;
                }
                g.setColor(new Color(color, color, color));

                // lifetime
                if (cells[x][y].lifetime > cellIsOld) {
                    int red = 255 - (int) (Math.random() * 150);
                    g.setColor(new Color(red, 0, 0));
                }
                g.fillRect(cells[x][y].x * cellWidthX, cells[x][y].y * cellWidthY, cellWidthX, cellWidthY);

            }
        }
    }

    /**
     * fill landscape with empty/dead cells
     */
    public void initLandscape() {
        if (generation == 0) {
            for (int x = 0; x <= gridSizeX; x++) {
                for (int y = 0; y <= gridSizeY; y++) {
                    cells[x][y] = new Cell();
                    cells[x][y].x = x;
                    cells[x][y].y = y;
                }
            }
        }
    }

    /**
     * bring a given amount of cellls to life
     */
    private void initCreatures() {
        if (generation == 0) {
            for (int i = 0; i <= liveCells; i++) {
                int x = getRandom(gridSizeX);
                int y = getRandom(gridSizeY);

                cells[x][y].dead = false;
            }
        }
    }

    /**
     * get random integer not > max
     *
     * @param max
     * @return
     */
    public int getRandom(int max) {
        int random = (int) (Math.random() * max);
        return random;
    }

    /**
     * draw frame
     *
     * @param g
     */
    public void drawGrid(Graphics g) {
        int cellWidthY = height / gridSizeY;

        g.setColor(new Color(20, 20, 20));

        // verticals
        for (int i = 0; i <= gridSizeX; i++) {
            g.drawLine(i * cellWidthY, 0, i * cellWidthY, height);
        }
        // horizontals
        for (int i = 0; i <= gridSizeY; i++) {
            g.drawLine(0, i * cellWidthY, width, i * cellWidthY);
        }
    }

    /**
     * life validation
     */
    private void handleLife() {
        mode.handleLife(this);
    }

    /**
     * create new cells
     */
    public void validateBirth(int x, int y) {
        if (cells[x][y].neighbors == birthcontrol) {
            cells[x][y].dead = false;
            cells[x][y].lifetime = 0;
        }
    }

    /**
     * let cells die because of underpopulation
     */
    public void validateUnderPopulation(int x, int y) {
        if (cells[x][y].neighbors < underpopulation) {
            cells[x][y].dead = true;
        }
    }

    /**
     * let cells die because of overpopulation
     */
    public void validateOverPopulation(int x, int y) {
        if (cells[x][y].neighbors > overpopulation) {
            cells[x][y].dead = true;
        }
    }

    /**
     * iterate through each cell and count it's neighbors
     */
    public void countNeighbors(int x, int y) {
        int toAdd = 0;

        if ((x >= 1 && x < gridSizeX - 1) && (y >= 1 && y < gridSizeY - 1)) {

            if (cells[x + 1][y].dead == false) {
                toAdd++;
            }
            if (cells[x + 1][y - 1].dead == false) {
                toAdd++;
            }
            if (cells[x][y - 1].dead == false) {
                toAdd++;
            }
            if (cells[x - 1][y - 1].dead == false) {
                toAdd++;
            }
            if (cells[x - 1][y].dead == false) {
                toAdd++;
            }
            if (cells[x - 1][y + 1].dead == false) {
                toAdd++;
            }
            if (cells[x][y + 1].dead == false) {
                toAdd++;
            }
            if (cells[x + 1][y + 1].dead == false) {
                toAdd++;
            }
        }

        cells[x][y].neighbors = toAdd;
    }

    /**
     * let the cell age
     */
    public void age(int x, int y) {
        if (cells[x][y].dead == false) {
            cells[x][y].lifetime++;
        }
    }

    private int countAliveCells() {
        int alive = 0;
        for (int x = 0; x < gridSizeX; x++) {
            for (int y = 0; y < gridSizeY; y++) {
                if (cells[x][y].dead == false) {
                    alive++;
                }
            }
        }
        return alive;
    }

    /**
     * debug output
     */
    private void debug() {

        // count live cells
        int alive = countAliveCells();

        // print out every 100th generation and living cell count
        if (generation % 100 == 0) {
            System.out.println("gen: " + generation + " | alive: " + alive);
        }
    }

    @Override
    public void run() {
        // init
        initLandscape();
        initCreatures();

        // debug output
        debug();

        // add generation
        generation++;

        // life validation
        handleLife();

        // well, ... , repaint
        repaint();

        // sleep for a given period of ticks
        try {
            Thread.sleep(sleep);
            run();
        } catch (InterruptedException ex) {
            System.out.println(ex.getMessage());
        }
    }

}
