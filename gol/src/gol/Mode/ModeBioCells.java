package gol.Mode;

import gol.Landscape;

/**
 *
 * @author alex
 */
public class ModeBioCells extends Mode {

    private Landscape l;
    
    @Override
    public void handleLife(Landscape l) {
        for (int x = 0; x < l.gridSizeX; x++) {
            for (int y = 0; y < l.gridSizeY; y++) {  
                l.countNeighbors(x, y);
                l.validateUnderPopulation(x, y);
                l.validateOverPopulation(x, y);
                l.validateBirth(x, y);
                //this.validateDeath(x,y);
            }
        }
    }

    @Override
    public void setup(Landscape l) {
        l.liveCells = 0;
        l.cellIsOld = 3;
    }
    
    private void validateDeath(int x, int y) {
        if(this.l.cells[x][y].lifetime > 5) {
            this.l.cells[x][y].lifetime = 0;
            this.l.cells[x][y].dead = true;
        }
    }
    
}
