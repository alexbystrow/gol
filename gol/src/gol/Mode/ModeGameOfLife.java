package gol.Mode;

import gol.Landscape;

/**
 *
 * @author alex
 */
public class ModeGameOfLife extends Mode {

    @Override
    public void handleLife(Landscape l) {
        for (int x = 0; x < l.gridSizeX; x++) {
            for (int y = 0; y < l.gridSizeY; y++) {
                l.countNeighbors(x, y);
            }
        }

        for (int x = 0; x < l.gridSizeX; x++) {
            for (int y = 0; y < l.gridSizeY; y++) {
                l.age(x, y);
                l.validateUnderPopulation(x, y);
                l.validateOverPopulation(x, y);
                l.validateBirth(x, y);
            }
        }
    }

    @Override
    public void setup(Landscape l) {
        
    }
    
}
