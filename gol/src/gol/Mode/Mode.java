package gol.Mode;

import gol.Landscape;

/**
 *
 * @author alex
 */
public abstract class Mode {
    public abstract void handleLife(Landscape l);
    public abstract void setup(Landscape l);
}
