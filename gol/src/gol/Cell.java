package gol;

/**
 *
 * @author bystrow
 */
public class Cell {
 
    public boolean dead = true;
    public int neighbors = 0;
    
    public int x;
    public int y;
    public int lifetime;
    
}
