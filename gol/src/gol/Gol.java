package gol;

import gol.Mode.*;
import javax.swing.JFrame;

/**
 *
 * @author bystrow
 */
public class Gol extends JFrame {

    private Landscape landscape;
    public final int width = 1600;
    public final int height = 800;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Gol gol = new Gol();
        gol.initFrame();
        gol.showFrame();
        gol.initLandscape();
    }

    public void initFrame() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(width + 3, height + 20);
    }

    public void initLandscape() {
        
        //Mode mode = new ModeBioCells();
        Mode mode = new ModeGameOfLife();
        
        // init
        landscape = new Landscape(mode, width, height);
        getContentPane().add(landscape);

        // run
        landscape.run();
    }

    public void showFrame() {
        setVisible(true);
    }

}
